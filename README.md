# Attributes Nfe #

Esse módulo é responsável por adicionar um campo chamado *nfe_number*
na tabela *sales_order*.
Esse campo será alimentado pelo erp para enviar o número da nota fiscal.