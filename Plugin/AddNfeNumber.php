<?php
namespace Avanti\AttributesNfe\Plugin;

use Magento\Sales\Api\Data\OrderExtensionFactory;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderSearchResultInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

class AddNfeNumber
{
    /**
     * Order Extension Attributes Factory
     *
     * @var OrderExtensionFactory
     */
    protected $orderExtensionFactory;


    /**
     * AddNfeNumber constructor.
     * @param OrderExtensionFactory $orderExtensionFactory
     */
    public function __construct(OrderExtensionFactory $orderExtensionFactory)
    {
        $this->orderExtensionFactory = $orderExtensionFactory;
    }

    /**
     * Insert number nfe in order Get API
     * @param OrderRepositoryInterface $subject
     * @param OrderInterface $order
     * @return OrderInterface
     */
    public function afterGet(OrderRepositoryInterface $subject, OrderInterface $order)
    {
        $nfeNumber = $order->getNfeNumber();

        if($nfeNumber != null) {
            $extensionAttributes = $order->getExtensionAttributes();
            $extensionAttributes = $extensionAttributes ? $extensionAttributes : $this->orderExtensionFactory->create();
            $extensionAttributes->setNfeNumber($nfeNumber);
            $order->setExtensionAttributes($extensionAttributes);
        }

        return $order;
    }

    /**
     * Inser Nfe number in all order list
     * @param OrderRepositoryInterface $subject
     * @param OrderSearchResultInterface $searchResult
     * @return OrderSearchResultInterface
     */
    public function afterGetList(OrderRepositoryInterface $subject, OrderSearchResultInterface $searchResult)
    {
        $orders = $searchResult->getItems();
        foreach ($orders as &$order) {
            $nfeNumber = $order->getNfeNumber();
            $extensionAttributes = $order->getExtensionAttributes();
            $extensionAttributes = $extensionAttributes ? $extensionAttributes : $this->orderExtensionFactory->create();
            $extensionAttributes->setNfeNumber($nfeNumber);
            $order->setExtensionAttributes($extensionAttributes);
        }

        return $searchResult;
    }

    /**
     * Se Nfe Number in Order when save by API
     * @param OrderRepositoryInterface $subject
     * @param OrderInterface $order
     * @return OrderInterface
     */
    public function afterSave(OrderRepositoryInterface $subject, OrderInterface $order)
    {
       $nfeNumber = $order->getExtensionAttributes()->getNfeNumber();
       $order->setNfeNumber($nfeNumber);
       $order->save();

       return $order;
    }
}
